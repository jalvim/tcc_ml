#############################################################
# SCRIPT DE EXTRAÇÃO DE FEATURES CONTIDAS EM ARQUIVOS CSV's #
#############################################################

import glob
import csv

def extFeat(csvPath):
    # FUNÇÃO RESPONSÁVEL POR EXTRAÇÃO DOS NOMES DE FEATURES.
    dct         = list(csv.DictReader(open(csvPath, "r")))
    ret         = dct[0].keys()
    return ret

def main():
    # FUNÇÃO PRINCIPAL DO SCIPT.
    csvPath     = "Hepa/data.csv"           # Extrai path do csv.
    data        = extFeat(csvPath)          # Extrai features de um determinado path.
    for item in data:                       # Percorre por cada feature identificada.
        print(item)                         # Imprime o nome associado a cada feature.

if __name__ == "__main__":                  # Verifiaca se o módulo em questão é o principal.
    main()                                  # Executa a função main().
