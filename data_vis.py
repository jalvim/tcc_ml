##############################################################################
#                    SCRIPT DE ALGORITMOS DE VIZUALIZAÇÃO                    #
##############################################################################

# ~~~~~~~~~~~~~~ IMPORTS DE OPERAÇÕES BÁSICAS ~~~~~~~~~~~~~~ 
import matplotlib.pyplot as plt
import unsup_methods.kmeans as km
import numpy as np
import csv
from numpy.linalg import norm
from scipy.cluster.hierarchy import linkage, dendrogram

def scatter_hist(x, y, ax, ax_histx, ax_histy):
    # Função de organização do plot de histogramas em si.
    ax_histx.tick_params(axis="x", labelbottom=False)
    ax_histy.tick_params(axis="y", labelleft=False)

    # the scatter plot:
    ax.scatter([x], [y])

    # now determine nice limits by hand:
    xymax = max(np.max(np.abs(x)), np.max(np.abs(y)))

    print(f"DEBUG: {x.T.shape}")
    ax_histx.hist(np.array(x))
    ax_histy.hist(np.array(y), orientation='horizontal')

def histo_plot(dataVec):
    # Função de plotagem de dados com histograma em cada dimensão.
    left, width = 0.1, 0.65
    bottom, height = 0.1, 0.65
    spacing = 0.005
    rect_scatter = [left, bottom, width, height]
    rect_histx = [left, bottom + height + spacing, width, 0.2]
    rect_histy = [left + width + spacing, bottom, 0.2, height]

    # start with a square Figure
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_axes(rect_scatter)
    ax_histx = fig.add_axes(rect_histx, sharex=ax)
    ax_histy = fig.add_axes(rect_histy, sharey=ax)
    x = dataVec[:, 270].T
    y = dataVec[:, 272].T

    scatter_hist(x, y, ax, ax_histx, ax_histy)
    fig.show()
    input()

def dend(dataVec):
    # Função de plotagem de dendograma
    link_data = linkage(dataVec, "single")
    fig = plt.figure()
    plt.title("Patient's dendrogram")
    dendrogram(
        link_data,
        color_threshold=1,
        show_leaf_counts=True
    )
    fig.show()
    input()

def main():
    # Criação de função principal para teste de plots.
    dataDict_comp = list(                           # Inicia Lista de dicionários.
        csv.DictReader(open("../Arrit/data.csv", "r"))    # Inicia Lista de dicionários.
    ) 
    coef = 0.2
    dataDict = km.sortElements(dataDict_comp, coef) 
    dataVec = km.orgVec(dataDict)                   # Organiza a lista de dicionários em lista de vetores.
    dend(dataVec)
    #histo_plot(dataVec)

    return

if __name__ == "__main__":
    main()
