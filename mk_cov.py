###########################################################################
#              SCRIPT DE EXTRAÇÃO DE MATRIZES DE COVARIÂNCIA              #
###########################################################################

import csv
import numpy as np

def mk_data_struct (file):
    ''' Função de formatação adequada de estrutura de dados.'''
    data = list(csv.DictReader(open(file, "r")))
    for ii, pac in enumerate(data):
        for key, val in pac.items():
            if val == "" or val == "False":
                data[ii][key] = 0
            elif val == "True":
                data[ii][key] = 1
            else:
                data[ii][key] = np.float(val)

    return data

def write_mat (out_file, cov_mat):
    ''' Função de escrita de matriz em arquivo na pasta de dados.'''
    cov_mat = np.matrix(cov_mat)
    with open(out_file, "wb") as file:
        for line in cov_mat:
            np.savetxt(file, line, fmt="%.3f")

    return

def main (file):
    ''' Função principal de extração de matriz de covariância do 
    conjunto de features em cada banco de dados.'''
    data = mk_data_struct(file)
    mat = np.array(
        [list(pac.values()) for pac in data],
    ).T
    cov_mat = np.corrcoef(mat)
    out_file = file.split(".csv")[0] + ".cov"
    write_mat(out_file, cov_mat)

    return cov_mat

if __name__ == "__main__":
    files = [
        #"../Arrit/data.csv",
        "../Breast/data.csv",
        "../Cervical/data.csv",
        "../Hepa/data.csv",
        "../Mamo/data_Mammography_modified.csv",
        "../PrimTum/data.csv"
    ]
    for file in files:
        cov = main(file)
        print(f"Arquivo: {file}")
        print()
        print(cov)
