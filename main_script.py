#####################################################################################
#              SCRIPT DE EXTRAÇÃO DE DADOS DE BASES DE DADOS QUAISQUER              #
#####################################################################################

# ~~~~~~~~~~~~~~ IMPORTS DE MANEJO DE DADOS ~~~~~~~~~~~~~~ 
from datapackage import Package as Pkg

# ~~~~~~~~~~~~~~ IMPORTS DE CLUSTERIZAÇÃO ~~~~~~~~~~~~~~ 
from pyclustering.cluster.center_initializer import kmeans_plusplus_initializer
from pyclustering.cluster import cluster_visualizer_multidim
from pyclustering.cluster.kmeans import kmeans
from pyclustering import cluster
import pyclustering as clust

# ~~~~~~~~~~~~~~ IMPORTS DE OPERAÇÕES BÁSICAS ~~~~~~~~~~~~~~ 
import numpy as np
import scipy as sci
import re
import csv
import glob 

def data2num(data):
    # FUNÇÃO RESPONSÁVEL POR TRANSFORMAR DADOS EM ESTRUTURAS DE NÚMERO.
    ret = []                                        # Cria estrutura de retorno como lista vazia.
    ret_it = []                                     # Cria estrutura de item para item de retorno.
    for vec in data:                                # Itera por cada vetor da lista.
        for item in vec:                            # Itera por cada item do vetor.
            cond1 = verIter(item)                   # Verifica se é string ou vetor.
            cond2 = (item is None)                  # Verifica se a var. é do tipo None.
            ret_it.append(item)                     # Iguala variável de retorno ao item.
            if cond1:                               # Verifica se algum valor é nulo.
                ret_it.pop()                        # Retira o último item da lista.
                ret_it.append(0)                    # Faz do item um número real.
            elif cond2:                             # verifica a condição de item vazio.
                ret_it.pop()                        # Retira o último item da lista.
                ret_it.append(0)                    # Adiciona caractere nulo.

        ret.append(ret_it)                          # Adiciona lista de valores ao retorno.
        ret_it = []                                 # Reinicia lista de itens.

    return ret                                      # Retorna a lista de números.

# AJUSTAR FUNÇÃO DE VISUALIZAÇÃO
def mkClustVis(clusters, sample):
    # FUNÇÃO RESPONSÁVEL POR CRIAR ESTRUTURA DE VISUALIZAÇÃO.
    visualizer = cluster_visualizer_multidim()      # Cria estrutura para visualização.
    visualizer.append_clusters(clusters, sample)    # Inclui os clusters e dados associados 
                                                    # à estrutura.
    visualizer.show()                               # Mostra a estrutura em si.

def verIter(item):
    # FUNÇÃO RESPONSÁVEL POR VERIFICAR SE ITEM É ITERÁVEL.
    try:                                            # Inicia cilo de exceção.
        iter(item)                                  # Verifica se a variável é iterável.
        return True                                 # Faz a variável de retorno verdadeira.
    except TypeError as tp:                         # Verifica exceção.
        return False                                # Faz a variável de retorno falsa

def mkSampleVec(dataDict, sample = None):
    # FUNÇÃO RESPONSÁVEL POR CLUSTERIZAR OS DADOS PELO MÉTODO K-MEANS.
    if sample is None:                              # Verifica se sample é inicializado nulo.
        ret = []                                    # Cria retorno como lista vazia.
    else:                                           # Caso contrário,
        ret = sample                                # faz o retorno igual a sample.
        
    for line in dataDict:                           # Itera sobre todas as linhas do dicionário
        vec = list(line.values())                   # Extrai valores do dict.
        for ii, item in enumerate(vec):             # Itera por termos no vetor.
            cond = verIter(item)                    # Verifica se a estrutura é iterável.
            if cond is True:                        
                if "" in item:                      # Verifica se o valor é string.
                    vec[ii] = 0                     # Faz variável booleana false.
                    break                           # Encerra o laço.
                else:                               # Caso contrário,
                    vec[ii] = 1                     # Faz variável booleana verdadeira.
                    break                           # Encerra o laço.
                
        ret.append(vec)                             # Adiciona ao fim da lista os valores do dict.

    return ret                                      # Retorna variável "ret".

def mkClusterKmeans(dataDict, flag=False):
    # FUNÇÃO RESPONSÁVEL POR CLUSTERIZAR OS DADOS PELO MÉTODO K-MEANS.
    dct = dataDict                                  # Cria vetor vazio p/ nova estrutura.
    #for vec in dataDict:                            # Itera por cada vetor do termo.
    #    dct.append(vec[0:1:-2])                     # Retira da estrutura termo de controle.

    sample = mkSampleVec(dct)                       # Organiza dados de forma lógica.
    sample = data2num(sample)                       # Faz da lista de amostras uma lista de números.

    # Inicializa pontos iniciais para algoritmo K-Means.
    initCenters = kmeans_plusplus_initializer(sample, 2).initialize()

    # Cria instância para processamento do algoritmo K-Means a partir dos pontos iniciais.
    kmeansIns = kmeans(sample, initCenters) 
    kmeansIns.process()                             # Executa a clusterização.
    clusters = kmeansIns.get_clusters()             # Extrai os clusters classificados.
    finalCenters = kmeansIns.get_centers()          # Extrai os novos centros estimados pelo alg.
    if flag is True:                                # Verifica se a flag de retorno é verdadeira
        return mkClustCSV(clusters, dataDict, flag=True)    # Retorna clusters já organizados.

    else:
        return clusters, finalCenters               # Retorna os clusters e novos centros extraídos.

def extFields(data):
    # FUNÇÃO PARA EXTRAÇÃO DE CAMPOS PARA NOME DAS FEATURES.
    features = []                                   # Inicia lista de features como vazia.
    for item in data:                               # Itera por cada item da lista.
        it = dict(item)                             # Cria dicionário a partir do item.
        features.append(it["name"])                 # Extrai o nome da feature.

    return features                                 # Retorna lista completa.

def org(resource):
    # FUNÇÃO RESPONSÁVEL POR ORGANIZAR OS DADOS.
    dataDict = []                                   # Inicia vetor de dados como lista vazia.
    if type(resource) is list:                      # Verifica se o tipo é dicionário.
        dataDict = resource                         # Faz da estrutura de dict como a entrada.
    else:                                           # Caso o contrário
        dataDict = resource.read(keyed=True)        # Lê a estrutura já pronta.

    return dataDict                                 # Retorna a lista gerada.

def mkCSV(data, inPath = None):
    # ORGANIZA DADOS COMO CSV
    if inPath is None:                              # Verifica se foi inserido string de nome.
        inPath = "data.csv"                         # Caso negativo, prepara string default.
    
    with open(inPath, "w", newline = False) as csvPath:              # Abre arquivo para escrita.
        # Cria objeto writer para a escrita.
        writer = csv.DictWriter(csvPath, fieldnames=data[0].keys())
        writer.writeheader()                        # Escreve o cabeçalho no arquivo.
        for item in data:                           # Itera por cada item da estrutura.
            writer.writerow(item)                   # Escreve os dados do dct. em c/ coluna.

def mkClustCSV(clusters, data, flag=False):
    # ORGANIZA DADOS EM TERMOS DE CLUSTERS EM ARQUIVO CSV.
    dct = []                                        # Inicia dicionário de output como vazio
    fields = list(data[0].keys())                   # Extrai lista com nome dos campos.
    fields.insert(0, "cluster")                     # Adiciona o campo "cluster" como campo.
    for ii, clust in enumerate(clusters):           # Itera sobre todos os clusters estimados.
        for idx in clust:                           # Itera por cada índice no cluster.
            vals = list(data[idx].values())         # Gera lista com valores do dicionário.
            vals.insert(0, ii + 1)                  # Adiciona o número do cluster na lista
            dct.append(dict(zip(fields, vals)))     # Adiciona novo dicionário na lista.

    if flag is True:                                # Verifica se flag de retorno é verdadeira.
        return dct                                  # Caso positivo, retorna dct.
    else:                                           # Caso negativo,
        mkCSV(dct, "data_clust.csv")                # gera arquivo CSV com a nova estrutura.

def main():
    # FUNÇÃO MAIN PARA EXTRAÇÃO DE DADOS.
    pack = Pkg("./Cervical/cervical.zip")           # Atribui o pacote à var. pack.

    for resource in pack.resources:                 # Itera sobre todos os recursos do pacote.

        # Verifica se o recurso em questão é a tabela de dados processados.
        if resource.descriptor["datahub"]["type"] == "derived/csv":
            # \/ \/ \/ VERIFICAR \/ \/ \/ 
            try:                                    # Tenta carregar a informação da internet.
                data = resource.read()              # Cria variável temp para dado.
            except:                                 # Em caso de excessão.
                # Lê informação diretamente do CSV de banco de dados.
                data = list(csv.DictReader(open("data.csv", "r")))
            
            # /\ /\ /\ VERIFICAR /\ /\ /\ 
            # Extrai vetor de features.
            features = extFields(
                resource.descriptor["schema"]["fields"]
            )
            data = org(resource)                    # Organiza os dados de forma inteligível.
            mkCSV(data)                             # Gera arquivo CSV.

            break                                   # Encerra laço.

    clusters, finalCenters = mkClusterKmeans(data)  # Performa K-Means sobre dados.
    sample = mkSampleVec(data)                      # Organiza as amostras. 
    #mkClustVis(clusters, sample)                   # Cria estrutura de visualização.
    mkClustCSV(clusters, data)                      # Prepara CSV de vizualização de clusters.

if __name__ == "__main__":                          # Verifica se é módulo principal
    main()                                          # Roda a função Main.
