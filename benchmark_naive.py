#############################################################################################
#                    SCRIPT DE IMPLEMENTAÇÃO BENCHMARKING P/ NAIVE BAYES                    #
#############################################################################################

# ~~~~~~~~~~~~~~ IMPORTS DE OPERAÇÕES BÁSICAS ~~~~~~~~~~~~~~ 
import numpy as np                                  # Imports das funções da biblioteca numpy.
import matplotlib.pyplot as plt                     # Import de módulo de plotagem.
from numpy.linalg import norm                       # Imports das funções da biblioteca numpy.
import csv                                          # Import de módulo de manuseio de docs csv.
import re                                           # Import de módulo de expressões regulares.

# ~~~~~~~~~~~~~~ IMPORTS DE MANIPULAÇÃO DE SISTEMA ~~~~~~~~~~~~~~ 
import os                                           # Módulos de manuseio de sistema.
import sys                                          # Módulos de manuseio de sistema.

# ~~~~~~~~~~~~~~ IMPORTS DE MÓDULO DE NAIVE BAYES ~~~~~~~~~~~~~~ 
import unsup_methods.naive_bayes as nai             # Importação do módulo naive bayes.

def mk_file_struct():
    # Função responsável por criar a estrutura com os arquivos e nomes.
    ret = {
        "Mamografia": "../Mamo/data_Mammography_modified.csv",
        #"Arritmia": "../Arrit/data.csv",
        "Câncer de mama": "../Breast/data.csv",
        "Tumor cervical": "../Cervical/data.csv",
        "Hepatite": "../Hepa/data.csv"
    }
    return ret

def mk_ac_struct(file_struct):
    # Função responsável pela implementação de uma estrutura com as acurácias.
    return {key: {"acc": [], "conf": []} for key in file_struct.keys()}

def plot_ac(iter_vec, ac):
    # Função responsável pela plotagem comparativa do desempenho em cada base.
    fig = plt.figure()
    for key, vals in ac.items():
        plt.plot(iter_vec, vals["acc"], label=key)

    plt.legend()
    plt.ylabel("Acurácia")
    plt.xlabel("Iteração")
    fig.show()

def plot_hist(data):
    # Plotagem de histograma da acurácia por base e método.
    fig = plt.figure()
    for key, acc in data.items():
        x_axis, y_axis = np.unique(acc["acc"], return_counts=True)
        plt.bar(x_axis, y_axis, label=key)

    plt.legend()
    plt.ylabel("Frequência")
    plt.xlabel("Acurácia")
    fig.show()

def print_acc(data):
    # Função de impressão da acurácia média.
    print("~~~~~~~~~ IMPRESSÃO DE ACURÁCIA ~~~~~~~~~") 
    for key, val in data.items():
        mu = np.mean(val["acc"])
        print(f"Acurácia média do método {key} = {mu}")
        print(f"Disperção da acurácia {np.std(val['acc'])}")
        print()

def print_mean_conf(data):
    # Função de impressão da matriz de confusão média para cada método.
    print("~~~~~~~~~ IMPRESSÃO DE MATRIZES DE CONFUSÃO ~~~~~~~~~") 
    for key, val in data.items():
            conf = np.zeros(data[key]["conf"][0].shape)
            for mat in data[key]["conf"]:
                try: 
                    conf += mat
                except Exception as error:
                    print("Erro: ", error, file=sys.stderr)
                    continue

            conf /= len(data[key]["conf"])
            print(f"Matriz de confusão média do método{key}:")
            print(conf)
            print()

def main():
    # Função principal do módulo de benchmarking
    file_struct = mk_file_struct()
    ac_struct = mk_ac_struct(file_struct)
    num_iter = 30
    iter_vec = list(range(num_iter))
    for key, file in file_struct.items():
        print(f"~~~~~~~~~~~~~~~~ {key} ~~~~~~~~~~~~~~~~")
        for ii in iter_vec:
            print(f"iter {ii+1} de {num_iter}")
            acc, conf = nai.main(file)
            ac_struct[key]["acc"].append(acc)
            ac_struct[key]["conf"].append(conf)

    plot_ac(iter_vec, ac_struct)
    #plot_hist(ac_struct)
    input()
    print_acc(ac_struct)
    print_mean_conf(ac_struct)

if __name__ == "__main__":
    main()
