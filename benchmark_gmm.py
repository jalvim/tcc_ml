#####################################################################################
#                    SCRIPT DE IMPLEMENTAÇÃO BENCHMARKING P/ GMM                    #
#####################################################################################

# ~~~~~~~~~~~~~~ IMPORTS DE OPERAÇÕES BÁSICAS ~~~~~~~~~~~~~~ 
import numpy as np                                  # Imports das funções da biblioteca numpy.
import matplotlib.pyplot as plt                     # Import de módulo de plotagem.
from numpy.linalg import norm                       # Imports das funções da biblioteca numpy.
import csv                                          # Import de módulo de manuseio de docs csv.
import re                                           # Import de módulo de expressões regulares.

# ~~~~~~~~~~~~~~ IMPORTS DE MANIPULAÇÃO DE SISTEMA ~~~~~~~~~~~~~~ 
from progress.bar import Bar                        # Módulos de barra de progresso.
import os                                           # Módulos de manuseio de sistema.
import sys                                          # Módulos de manuseio de sistema.

# ~~~~~~~~~~~~~~ IMPORTS DE MÓDULO DE NAIVE BAYES ~~~~~~~~~~~~~~ 
import unsup_methods.gauss_mix as gmm               # Importação do módulo naive bayes.

def plot_acc(acc_dct):
    # Função de plotagem de valores obtidos para acurácia em cada base.
    fig = plt.figure()
    for key, acc in acc_dct.items():
        iter_vec = list(range(len(acc)))
        plt.plot(iter_vec, acc, label=key)

    plt.legend()
    plt.ylabel("Acurácia global")
    plt.xlabel("Iteração")
    fig.show()

def plot_hist(data):
    # Plotagem de histograma da acurácia por base e método.
    fig = plt.figure()
    for key, acc in data.items():
        x_axis, y_axis = np.unique(acc, return_counts=True)
        plt.bar(x_axis, y_axis, label=key)

    plt.legend()
    plt.ylabel("Frequência")
    plt.xlabel("Acurácia")
    fig.show()

def print_acc(data):
    # Função de impressão da acurácia média.
    for key, val in data.items():
        mu = np.mean(val)
        print(f"Acurácia média do método {key} = {mu}")
        print(f"Disperção da acurácia {np.std(val)}")
        print()

def print_conf(data):
    # Função de impressão da matriz de confusão
    for key, val in data.items():
        conf = np.zeros(data[key][0].shape)
        for mat in val:
            try:
                conf += mat
            except Exception as error:
                print("ERRO: ", error, file=sys.stderr)
                continue

        conf /= len(val)
        conf = conf / conf.sum().sum()
        print(f"Matriz de confusão do método {key}:")
        print(conf)
        print()

def main(csv_list, num_iter):
    # Define a função principal do programa.
    acc_dct = {key: [] for key in csv_list.keys()}
    conf_dct = {key: [] for key in csv_list.keys()}
    max_bar = num_iter * len(csv_list)              # Avalia tamanho da barra de progresso.
    with Bar("Benchmarking...", max=max_bar) as bar:
        for key, arq in csv_list.items():           # Itera por cada arquivo da lista.
            for ii in range(num_iter):              # Itera pelo número de iterações.
                acc, conf_mat = gmm.main(arq)
                acc_dct[key].append(acc)
                conf_dct[key].append(conf_mat)
                print()                             # DEBUG
                print("DEBUG: conf_mat:")           # DEBUG
                print(conf_mat)                     # DEBUG
                bar.next()

    plot_acc(acc_dct)                               # Plota restultados obtidos pra cada base de dados.
    plot_hist(acc_dct)
    input()
    print_acc(acc_dct)
    print_conf(conf_dct)

if __name__ == "__main__":
    num_iter = 30
    csv_list = {
        #"Arritmia": "../Arrit/data.csv",
        "Câncer de mama": "../Breast/data.csv",
        "Tumor cervical": "../Cervical/data.csv",
        "Mamografia": "../Mamo/data_Mammography_modified.csv",
        "Hepatite": "../Hepa/data.csv",
    }
    main(csv_list, num_iter)
