###################################################################################
#                    SCRIPT DE BENCHMARK DE DIFERENTES MÉTODOS                    #
###################################################################################

# ~~~~~~~~~~~~~~ IMPORTS DE MÓDULOS DESENVOLVIDOS ~~~~~~~~~~~~~~ 
import unsup_methods.kmeans as km
import unsup_methods.hierarchical_KMeans as hierar

# ~~~~~~~~~~~~~~ IMPORTS DE MÓDULOS BÁSICOS ~~~~~~~~~~~~~~ 
from pyclustering.cluster.center_initializer import kmeans_plusplus_initializer
from pyclustering.cluster import cluster_visualizer_multidim
from pyclustering.cluster.kmeans import kmeans
from pyclustering import cluster
from progress.bar import Bar
import pyclustering as clust
import matplotlib.pyplot as plt
import numpy as np
import csv
import sys
import os 

def eval_method(init_form, dataVec, num_clust, flag):
    # Função de avalisação de método de inicialização.
    if flag == True:
        initCenters = init_form(dataVec)
    else:
        initCenters = init_form(dataVec["features"], num_clust)

    clust, _ = km.kMeans_process(dataVec["features"], initCenters)
    acc, conf_mat = km.calc_acc(clust, dataVec, num_clust)
    #print("Matriz de confusão:")
    #print(conf_mat)
    return acc, conf_mat

def mkPlot(data, xlabel, ylabel):
    # Função de desenho específico de subplot.
    fig = plt.figure()
    for key, acc in data.items():
        iter_vec = list(range(len(data[key])))
        plt.plot(iter_vec, data[key], label=key)

    plt.legend()
    plt.ylabel(ylabel)
    plt.xlabel(xlabel)
    fig.show()

def plot_hist(data):
    # Plotagem de histograma da acurácia por base e método.
    fig = plt.figure()
    for key, acc in data.items():
        x_axis, y_axis = np.unique(acc, return_counts=True)
        plt.bar(x_axis, y_axis, label=key)

    plt.legend()
    plt.ylabel("Frequência")
    plt.xlabel("Acurácia")
    fig.show()

def print_acc(data):
    # Função de impressão da acurácia média.
    for key, val in data.items():
        try: 
            mu = np.mean(val)
            print(f"Acurácia média do método {key} = {mu}")
            print(f"Disperção da acurácia {np.std(val)}")
            print()
        except Exception as error:
            print("ERRO: ", error, file=sys.stderr)
            continue

def print_conf(data):
    ## Função de impressão da matriz de confusão
    #for key, val in data.items():
    #    conf = np.zeros(data[key][0].shape)
    #    for mat in val:
    #        try:
    #            conf += mat
    #        except Exception as error:
    #            print("ERRO: ", error, file=sys.stderr)
    #            continue

    #    conf /= len(val)
    #    conf /= conf.sum().sum()
    #    print(f"Matriz de confusão do método {key}:")
    #    print(conf)
    #    print()
    for key, val in data.items():
        conf = val
        conf /= conf.sum().sum()
        print(f"Matriz de confusão do método {key}:")
        print(conf)
        print()

def main(dataVec_comp, dataDict_comp):
    # Incia bateria de testes na função principal.
    init_vec = [                                    # Inicia vetor de inicializações.
        km.kmeans_plusplus_init,
        #km.recInit,
        km.supInit,
        km.randInit,
        hierar.hierar_init_avg
    ]
    num_clust = int(max(np.unique(dataVec_comp["classes"]))) + 1
    num_iter = 30
    coef = 0.2
    acc_data = {
        "k_means_plusplus": [],
        #"recInit": [],
        "supInit": [],
        "randinit": [],
        "hierar_init_avg": []
    } 
    conf_data = {
        "k_means_plusplus": np.zeros((num_clust, num_clust)),
        #"recInit": np.zeros((num_clust, num_clust)),
        "supInit": np.zeros((num_clust, num_clust)),
        "randinit": np.zeros((num_clust, num_clust)),
        "hierar_init_avg": np.zeros((num_clust, num_clust))
    }
    max_bar = num_iter * len(init_vec)
    with Bar("Benchmarking...", max=max_bar) as bar:
        for init_form, key in zip(init_vec, acc_data.keys()):
            for ii in range(num_iter):
                flag = False
                if key == "supInit":
                    flag = True

                dataVec = km.orgVec(
                    km.sortElements(dataDict_comp, coef)
                )
                acc, conf_mat = eval_method(init_form, dataVec, num_clust, flag)
                acc_data[key].append(acc)
                conf_data[key] += conf_mat
                bar.next()

    mkPlot(acc_data, "Iteração", "Acurácia")
    plot_hist(acc_data)
    input()
    print_acc(acc_data)
    print_conf(conf_data)

if __name__ == "__main__":                          # Verifica se o módulo inicializado é o principal.
    data_files = [
        #"../Mamo/data_Mammography_modified.csv",
        "../Breast/data.csv",
        "../Cervical/data.csv",
        "../Hepa/data.csv"
    ]
    for file in data_files:
        dataDict_comp = list(csv.DictReader(        # Inicia Lista de dicionários como glabal.
            open(file, "r"))                        # Inicia Lista de dicionários como glabal.
        ) 
        dataVec_comp = km.orgVec(dataDict_comp)
        main(dataVec_comp, dataDict_comp)           # Chama função principal.
