#############################################################################
#                    SCRIPT DE IMPLEMENTAÇÃO NAIVE BAYES                    #
#############################################################################

# ~~~~~~~~~~~~~~ IMPORTS DE OPERAÇÕES BÁSICAS ~~~~~~~~~~~~~~ 
from progress.bar import Bar                        # Importa classe de Barra de status.
from random import randint                          # Importa função "randint" do módulo random.
import numpy as np                                  # Imports das funções da biblioteca numpy.
from numpy.linalg import norm                       # Imports das funções da biblioteca numpy.
import scipy as sci                                 # Import do módulo scipy.  
import csv                                          # Import de módulo de manuseio de docs csv.
import re                                           # Import de módulo de expressões regulares.

# ~~~~~~~~~~~~~~ IMPORTS DE MANIPULAÇÃO DE SISTEMA ~~~~~~~~~~~~~~ 
import os                                           # Módulos de manuseio de sistema.
import sys                                          # Módulos de manuseio de sistema.

def mk_dct(csv_file):
    # Função responsável pela escrita da estrutura de dct utilizada.
    ret = list(
        csv.DictReader(open(csv_file, "r"))         # Inicia Lista de dicionários.
    )

    return ret

def sort_idxs(coef, tam):
    # Função de sorteio de índices para x-validation.
    idxs = []                                       # Inicia lista de índices.
    num_idx = np.floor(coef*(tam - 1))              # Encontra o número de coficientes do novo dct.
    for ii in range(np.int(num_idx)):               # Itera por cada termo da lista de pacientes.
        tmp = randint(0, tam-1)                     # Sorteia índice aleatório.
        while tmp in idxs:                          # Enquanto tmp está na lista de índices, ...
            tmp = randint(0, tam-1)                 # ... sorteia índice aleatório.

        idxs.append(tmp)                            # Acrescenta o índice sorteado no vetor idxs.
        
    return idxs

def orgVec(dataDict):
    # Função responsável por organizar o dicionário com features em valores numéricos.
    dataVec = {field: [] for field in dataDict.keys()}  # Inicia estrutura de retorno como lista vazia.
    for jj, vec in enumerate(dataDict["features"]):     # Itera por cada dicionário da lista.
        for ii, val in enumerate(vec):                  # Itera por termos do vetor de valores.
            if val is "": 
                vec[ii] = 0                             # Substitui caractere vazio por zero.
            else: 
                if val == "False" or val == "True":
                    if val == "False": 
                        vec[ii] = int(0)
                    else: 
                        vec[ii] = int(1)

                    continue

                vec[ii] = float(val)                    # Caso contrário, torna texto em float.

        if dataDict["class"][jj] == "":
            dataDict["class"][jj] = int(0)
        else:
            dataDict["class"][jj] = int(
                dataDict["class"][jj]
            )
        
        dataVec["features"].append(np.array(vec))       # Apenda o vetor numérico ao fim da lista.

    dataVec["features"] = np.matrix(dataVec["features"])
    dataVec["class"] = np.array(dataDict["class"])

    return dataVec                                      # Retorna Lista de listas como lista de vetores numéricos.

def parse_data(data_dict, part=False, coef=0.2):
    # Função de organização dos dados do dicionário original
    ret_data = {
        "features": [],
        "class": []
    }
    tam = len(data_dict)                                # Estima o tamanho total da estrutura.
    if part == True: 
        idxs = sort_idxs(coef, tam)
    else: 
        idxs = range(tam)

    for idx in idxs:
        ret_data["features"].append(
            list(data_dict[idx].values())[0:-2]
        )
        ret_data["class"].append(
            list(data_dict[idx].values())[-1]
        )

    ret_data = orgVec(ret_data)

    return ret_data

def calc_prob_class(class_vec):
    # Calcula a probabiliadade de cada Classe.
    probs = []
    _, counts = np.unique(
        class_vec, return_counts=True
    )
    tam = len(class_vec)
    for num in counts:
        probs.append(num/tam)

    return probs

def get_stat(data_dict):
    # Função de obtenção dos parâmetros estatísticos por classe.
    classes = np.unique(data_dict["class"])
    mu = [None]*len(classes)
    sig = [None]*len(classes)
    dct_tmp = {cla: [] for cla in classes}
    for jj, cla in enumerate(classes):
        for ii in range(data_dict["features"].shape[0]):
            if np.float(data_dict["class"][ii]) == np.float(cla):
                dct_tmp[cla].append(
                    data_dict["features"][ii,:]
                )

        if len(dct_tmp[cla]) != 0:
            mu[jj] = np.mean(dct_tmp[cla], axis=0)
            sig[jj] = np.std(dct_tmp[cla], axis=0)
        else:
            mu[jj] = np.zeros(data_dict["features"][ii,:].shape)
            sig[jj] = np.zeros(data_dict["features"][ii,:].shape)

    return mu, sig

def gauss_prob(pac, prob_class, mu, sig):
    # Calcula prob. condicional com base em distribuição gaussiana.
    prob = prob_class
    for ii, feat in enumerate(pac):
        if sig[ii] == 0: sig[ii] = 1
        prob *= np.exp(
            -np.power(pac[ii] - mu[ii], 2)/(2*sig[ii]*sig[ii])
        )
        prob /= np.sqrt(2*np.pi*np.power(sig[ii], 2))

    return prob

def accuracy(result, gab):
    # Função de medição de acurácia da classificação.
    tam = len(gab)
    score = 0
    for cla_res, cla_gab in zip(result, gab):
        if cla_res == cla_gab:
            score += 1

    return (score/tam)*100

def conf_mat(result, gab):
    # Funçãoresponsável pela elaboração matriz de confusão.
    class_vec = np.unique(result)
    num_class = len(class_vec)
    confusion = np.zeros((num_class, num_class))
    for cla_res, cla_gab in zip(result, gab):
        x_idx = np.where(class_vec == cla_res)
        y_idx = np.where(class_vec == cla_gab)
        confusion[x_idx, y_idx] += 1

    tot = confusion.sum().sum()

    return confusion / tot

def separate_data(red, gab):
    # Função de separação dos dados de treinamento e de teste.
    tam = red["features"].shape[0]
    for ii in range(tam):
        pac = red["features"][ii,:]
        pac = np.array(pac)[0]
        if pac in gab["features"]:
            idx = np.where(
                (gab["features"] == pac).all(1)
            )[0]
            gab["features"] = np.delete(
                gab["features"], 
                (idx), 
                axis=0
            )
            gab["class"] = np.delete(
                gab["class"], 
                (idx), 
                axis=0
            )

    return gab

def WSSQ_measure(data):
    # Função responsável por extrair a métrica WSSQ dos resultados.
    class_vec, counts = np.unique(data["class"], return_counts=True)
    wssq = {cla: 0 for cla in class_vec}
    centroids = wssq
    for ii in range(data["features"].shape[0]):
        print(f"DEBUG: {ii} {data['features'].shape}")
        pac = data["features"][ii,:]
        cla = data["class"][ii,:]
        idx = class_vec.index(cla)
        centroids[cla] += (pac / counts[idx])

    for ii in range(data["features"].shape[0]):
        pac = data["features"][ii,:]
        cla = data["class"][ii,:]
        tmp = norm(pac - centroids[cla])
        wssq[cla] += np.square(tmp)

    return wssq

def main(csv_file):
    # Implementa função principal do script.
    data_dct_comp = mk_dct(csv_file)
    data_dct_red = parse_data(data_dct_comp, part=True, coef=0.3)
    gabarito = parse_data(data_dct_comp)
    prob_classes = calc_prob_class(data_dct_red["class"])
    mu, sig = get_stat(data_dct_red)
    classes = np.unique(data_dct_red["class"])
    result = {"features": [], "class": []}
    with Bar("Processing...", max=gabarito["features"].shape[0]) as bar:
        for ii in range(gabarito["features"].shape[0]):
            pac = gabarito["features"][ii,:]
            pac = np.array(pac)[0]
            probs = []
            for jj, p_class in enumerate(prob_classes):
                probs.append(
                    gauss_prob(pac, p_class, mu[jj][0], sig[jj][0])
                )

            idx = np.argmax(probs)
            result["features"].append(pac)
            result["class"].append(classes[idx])
            bar.next()

    result["features"] = np.concatenate(result["features"], axis=0) 
    result = separate_data(data_dct_red, result)
    gabarito = separate_data(data_dct_red, gabarito)
    #WSSQ = WSSQ_measure(result)
    ac = accuracy(result["class"], gabarito["class"])
    confusion = conf_mat(result["class"], gabarito["class"])
    print("Acurácia: %.2f"%ac)
    print()
    print(f"{confusion}")
    #for cla in WSSQ.keys:
    #    print("WSSQ no clust %d: %.2f "%cla%WSSQ[cla])

    return ac, confusion

if __name__ == "__main__":
    #csv_file = "../../Mamo/data_Mammography_modified.csv"
    #csv_file = "../../Arrit/data.csv"
    #csv_file = "../../Breast/data.csv"
    csv_file = "../../Cervical/data.csv"
    #csv_file = "../../Hepa/data.csv"

    main(csv_file)
