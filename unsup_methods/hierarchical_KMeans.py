from pyclustering.cluster import cluster_visualizer_multidim
from random import randint
from numpy.linalg import norm

if __name__ == "__main__":
    import kmeans as km
else:
    import unsup_methods.kmeans as km

import numpy as np
import csv
import re

def clust_init(dataVec):
    # Função de inicialização de lista de vetores.
    clust = [] 
    for ii in range(dataVec.shape[0]):
        clust.append(dataVec[ii,:])

    return clust

def hierar_init_avg(dataVec, num_clust):
    # Função de processo do método de classificação hirárquico.
    clust = clust_init(dataVec)                     # Inicia clusters como lista dos pacientes.
    ii = 0                                          # Cria o índice de controle.
    while len(clust) != num_clust:                  # Itera enquanto a quantidade de clusters n seja a ideal.
        p1 = clust[ii]                              # Define o paciente de referência da iteração.
        dist = np.full([len(clust), 1], np.Inf)
        for jj, pac in enumerate(clust):
            if ii == jj: continue
            dist[jj] = norm(
               p1 - pac 
            )

        idx = np.argmin(dist)
        tmp = clust[idx] 
        clust[ii] = np.mean(
            [tmp, clust[ii]],
            axis=0
        )
        del clust[idx]
        ii = (ii + 1) % len(clust)

    return np.concatenate(clust, axis=0)             # Retorna centros iniciais estipulados. 

def hierar_init_mindist(dataVec, num_clust):
    # Função de processo do método de classificação hirárquico.
    clust = clust_init(dataVec)                     # Inicia clusters como lista dos pacientes.
    while len(clust) != num_clust:                  # Itera enquanto a quantidade de clusters n seja a ideal.
        dist = np.full(np.size(clust), np.inf)
        for ii in range(dataVec.shape[0]):
            p1 = dataVec[ii,:]
            for kk, clus in enumerate(clust):
                for jj in range(clus.shape[0]):
                    p_comp = clus[jj,:]
                    if (p1 == p_comp).all(): continue
                    d = norm(p1 - p_comp)
                    if d < norm(dist[ii]):
                        dist[kk] = d

            idx = np.argmin(dist)
            print(dist)
            print(idx)
            np.concatenate([clust[idx], p1], axis=0)

    for ii in range(len(clust)):
        clust[ii] = np.mean(clust[ii], axis=0)

    return np.concatenate(clust, axis=0)             # Retorna centros iniciais estipulados. 

def mkClustVis(clusters):
    # FUNÇÃO RESPONSÁVEL POR CRIAR ESTRUTURA DE VISUALIZAÇÃO.
    center = {}                                     # Inicia objeto sample como dct vazio.
    for ii, clust in enumerate(clusters):
        center.update({f"center_{ii}":
            np.mean(clust, axis=0) 
        })

    visualizer = cluster_visualizer_multidim()      # Cria estrutura para visualização.
    visualizer.append_clusters(
        list(center.values()), list(clusters)       # Inclui os clusters e dados associados ...
    )
                                                    # ... à estrutura.
    visualizer.show()                               # Mostra a estrutura em si.

def main():
    # Função principal do script.
    coef = 0.2                                      # Cria variável de coeficiente de aproveitamento de pacientes.
    num_clust = 3                                   # Inicia número de clusters.
    dataDict_comp = list(
        csv.DictReader(open("../../Arrit/data.csv", "r"))   # Inicia Lista de dicionários.
    )
    dataDict = km.sortElements(dataDict_comp, coef) # Sorteia elementos a serem usados.
    dataVec = km.orgVec(dataDict)                   # Organiza o dicionário como objeto matricial.
    initCenters = hierar_init_avg(dataVec, num_clust)   # Processa a classificação pelo método hierárquico.
    #initCenters = hierar_init_mindist(dataVec, num_clust)   # Processa a classificação pelo método hierárquico.
    clusters, clusters_idx = km.kMeans_process(dataVec, initCenters)    # Processa a clusterização do K-Means com centros encontrados.
    clusters_idx = km.apply_in_all_data(dataDict_comp, clusters)        # Aplica centros encontrados no DB inteiro.
    km.mkClustCSV(clusters_idx, dataDict_comp)                          # Gera arquivo CSV com gabarito.
    #km.mkClustCSV(clusters_idx, dataDict)                               # Gera arquivo CSV com gabarito.
    #km.sys_print(num_clust, dataVec.shape[0])                           # Calcula as métricas de acurácia da clusterização.
    #km.sys_print(num_clust, len(dataDict_comp))                         # Calcula as métricas de acurácia da clusterização.
    #mkClustVis(clusters)                                                # Cria vizualização dos clusters.
    km.sys_print_arrit(num_clust, len(dataDict_comp))                    # Calcula as métricas de acurácia da clusterização.

if __name__ == "__main__":
    main()
