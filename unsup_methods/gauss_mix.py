##################################################################################
#                    SCRIPT DE IMPLEMENTAÇÃO GAUSSIAN MIXTURE                    #
##################################################################################

# ~~~~~~~~~~~~~~ IMPORTS DE OPERAÇÕES BÁSICAS ~~~~~~~~~~~~~~ 
from scipy.stats import multivariate_normal         # Import do módulo scipy.  
from progress.bar import Bar                        # Importa classe de Barra de status.
from random import randint                          # Importa função "randint" do módulo random.
from numpy.linalg import norm                       # Imports das funções da biblioteca numpy.
import matplotlib.pyplot as plt                     # Import de módulo de plotagem.
import numpy as np                                  # Imports das funções da biblioteca numpy.
import csv                                          # Import de módulo de manuseio de docs csv.
import re                                           # Import de módulo de expressões regulares.

# ~~~~~~~~~~~~~~ IMPORTS DE MANIPULAÇÃO DE SISTEMA ~~~~~~~~~~~~~~ 
import os                                           # Módulos de manuseio de sistema.
import sys                                          # Módulos de manuseio de sistema.

# ~~~~~~~~~~~~~~ IMPORTS DE MÓDULOS JÁ IMPLEMENTADOS ~~~~~~~~~~~~~~ 
if __name__ == "__main__":
    import kmeans as km
    import naive_bayes as nb
else:
    import unsup_methods.kmeans as km
    import unsup_methods.naive_bayes as nb

def calc_num_class(class_vec):
    # Função usada para a determinação do número de classes.
    vec = np.unique(class_vec)

    return len(vec)

def init_gauss(feat_vec, num_clust):
    # Função de inicialização das gaussianas usadas.
    tam = feat_vec.shape[0]
    ret_list = []
    ii = 0
    pac_pas = np.zeros(feat_vec[0,:].shape)
    while ii < num_clust:
        idx = np.random.randint(0, high=(tam - 1))
        pac = feat_vec[idx,:]
        if pac_pas.sum() != pac.sum():
            ret_list.append({
                "mu": pac,
                "cov": np.identity(
                    feat_vec.shape[1]
                ),
                "pacientes": [],
                "prob": []
            })
            ii += 1

    return ret_list

def calc_cov_mat(data, mu):
    # Função responsável pelo cálculo inicial da covariância
    tam = data.shape[0]
    sq_tam = mu.shape[1]
    cov = np.zeros((sq_tam, sq_tam))
    for ii in range(tam):
        tmp_mat = np.matmul(
            (data[ii,:] - mu).T,
            (data[ii,:] - mu)
        )
        cov += tmp_mat

    cov /= sq_tam
    if np.linalg.det(cov) < 1e-25:
        cov = np.identity(sq_tam)

    return cov

def sup_init(data):
    # Função de inicialização de distribuição supervisionada.
    ret_list = []
    classes = np.unique(data["class"])
    for cla in classes:
        idxs = np.where(data["class"] == cla)[0]
        mu = np.zeros(data["features"][0,:].shape)
        #cov = np.identity(
        #    data["features"].shape[1]
        #)
        cov = calc_cov_mat(data["features"], mu)
        for idx in idxs:
            mu += data["features"][idx,:]

        mu /= len(idxs)
        ret_list.append({
            "mu": mu,
            "cov": cov,
            "pacientes": [],
            "prob": []
        })

    return ret_list

def expec_step(pi_c, gauss_vec, pac):
    # Função responsável por calcular a etapa de esperança do alg. EM.
    r_ic = [None] * len(gauss_vec)
    for ii, gauss in enumerate(gauss_vec):
        cov = gauss["cov"]
        mu = gauss["mu"].tolist()
        mn = multivariate_normal(
            mu[0], 
            cov
        )
        r_ic[ii] = pi_c[ii] * mn.pdf(pac)

    sum_r = sum(r_ic)
    if sum_r != 0:
        r_ic /= sum_r

    return r_ic

def max_step(gauss, r_ic, num_pac):
    # Função responsável por calcular a etapa de maximização do alg. EM.
    m_c = sum(r_ic)
    mu = np.zeros(gauss["mu"].shape)
    for ii, pac in enumerate(gauss["pacientes"]):
        if m_c == 0:
            continue
        else:
            mu += sum(np.multiply(pac, r_ic[ii])) / m_c

    pi_c = m_c / num_pac
    cov = np.zeros(gauss["cov"].shape)
    for ii in range(len(gauss["pacientes"])):
        if m_c != 0:
            const = r_ic[ii] / m_c
        else:
            const = 0

        cov += np.multiply(
            const,
            np.matmul(
                np.transpose(gauss["pacientes"][ii] - mu),
                gauss["pacientes"][ii] - mu
            )
        )

    return mu, cov, pi_c

def loglike_calc(gauss, pi_c):
    # Função de cálculo da função de máxima verossimilhança.
    cov = gauss["cov"]
    mu = gauss["mu"].tolist()
    mn = multivariate_normal(mu[0], cov)
    loglike = 0
    for pac, k in zip(gauss["pacientes"], pi_c):
        arg = k * mn.pdf(pac)
        if arg != 0:
            loglike += np.log10(arg)

    return loglike

def accuracy(gauss, data_dct_red):
    # Função para cálculo e percepção de acurácia do sistema.
    classes = np.unique(data_dct_red["class"])
    class_vec = {cla: 0 for cla in classes}
    conf = [0] * len(classes)
    for pac in gauss["pacientes"]:
        idx = np.where(
            (data_dct_red["features"] == pac).all(1)
        )[0]
        cla = data_dct_red["class"][idx]
        class_vec[cla[0]] += 1
        conf[cla[0] - 1] += 1

    max_val = np.max(list(class_vec.values()))
    tot = np.sum(list(class_vec.values()))
    acc = max_val / tot
    comp = tot - max_val
    cla = np.argmax(list(class_vec.values()))
    cla = list(class_vec.keys())[cla] 
    return acc, comp, tot, cla, conf

def print_ac(gauss_vec, data_dct_red, print_flag=False):
    # Função de impressão de medidas de acurácia por cluster.
    conf_mat = []
    for gauss in gauss_vec:
        acc, comp, tot, cla, conf = accuracy(gauss, data_dct_red)
        conf_mat.append(conf)
        if print_flag:
            print()
            print(f"Acurácia: {acc}")
            print(f"Falsos positivos: {comp}")
            print(f"Número total: {tot}")
            print(f"Classe: {cla}")

    return acc, np.matrix(conf_mat)

def plot_log(loglike):
    # Função responsável pela plotagem da trilha de verossimilhança.
    iter_vec = list(range(len(loglike)))
    fig = plt.figure()
    plt.plot(iter_vec, loglike)
    plt.title("Loglikelihood per iteration")
    plt.ylabel("Loglikelihood")
    plt.xlabel("Iteration")
    fig.show()

    input()
    return

def main(csv_file, print_flag=False):
    # Função principal da implementação do programa.
    max_iter = 50
    data_dct_comp = nb.mk_dct(csv_file)
    data_dct_red = nb.parse_data(data_dct_comp, part=True, coef=0.7)
    gabarito = nb.parse_data(data_dct_comp)
    num_pac = gabarito["features"].shape[0]
    num_clust = calc_num_class(data_dct_red["class"])
    log_likelihoods = [] 
    #gauss_vec = init_gauss(data_dct_red["features"], num_clust)
    gauss_vec = sup_init(data_dct_red)
    pi_c = np.ones((gabarito["features"].shape[0], num_clust))
    r_ic = np.zeros((gabarito["features"].shape[0], num_clust))
    error = 1
    jj = 0
    while error > 1e-12 and jj < max_iter:
        jj += 1
        for ii in range(len(gauss_vec)):
            del gauss_vec[ii]["pacientes"][:]
            del gauss_vec[ii]["prob"][:]

        # Etapa de ESPERANÇA do algoritmo EM.
        for ii, pac in enumerate(gabarito["features"]):
            r_ic[ii,:] = expec_step(pi_c[ii], gauss_vec, pac)
            idx = np.argmax(r_ic[ii,:])
            gauss_vec[idx]["pacientes"].append(pac)
            gauss_vec[idx]["prob"].append(r_ic[ii,idx])

        # Etapa de MAXIMIZAÇÃO do algoritmo EM
        for ii in range(num_clust):
            gauss_vec[ii]["mu"], gauss_vec[ii]["cov"], pi_c[:,ii] = max_step(
                gauss_vec[ii], 
                gauss_vec[ii]["prob"],
                num_pac
            )
            cov = gauss_vec[ii]["cov"]
            # Reinicia matriz de covariância em caso singular.
            if np.linalg.det(cov) <= 1e-25:
                gauss_vec[ii]["cov"] = np.identity(cov.shape[0])

        log_likelihoods.append(
            sum([
                loglike_calc(gauss, pi_c[:,ii]) for ii, gauss in enumerate(gauss_vec)
            ])
        )
        #print(f"DEBUG loglike: {log_likelihoods[-1]} ii: {jj}")
        if np.isinf(log_likelihoods[-1]):
            continue

        if len(log_likelihoods) >= 2:
            error = abs(log_likelihoods[-1] - log_likelihoods[-2])
        else:
            error = abs(log_likelihoods[-1])

    if print_flag:
        plot_log(log_likelihoods)

    acc, conf_mat = print_ac(gauss_vec, gabarito, print_flag)
    return acc, conf_mat

if __name__ == "__main__":
    #csv_file = "../../Mamo/data_Mammography_modified.csv"
    #csv_file = "../../Arrit/data.csv"
    #csv_file = "../../Breast/data.csv"
    #csv_file = "../../Cervical/data.csv"
    csv_file = "../../Hepa/data.csv"
    main(csv_file, print_flag=True)
