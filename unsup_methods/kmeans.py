##################################################################################
#                    SCRIPT DE IMPLEMENTAÇÃO BÁSICA DE KMEANS                    #
##################################################################################

# ~~~~~~~~~~~~~~ IMPORTS DE OPERAÇÕES BÁSICAS ~~~~~~~~~~~~~~ 
from random import randint                          # Importa função "randint" do módulo random.
import numpy as np                                  # Imports das funções da biblioteca numpy.
from numpy.linalg import norm                       # Imports das funções da biblioteca numpy.
import scipy as sci                                 # Import do módulo scipy.  
import csv                                          # Import de módulo de manuseio de docs csv.
import re                                           # Import de módulo de expressões regulares.

# ~~~~~~~~~~~~~~ IMPORTS DE MANIPULAÇÃO DE SISTEMA ~~~~~~~~~~~~~~ 
import os                                           # Módulos de manuseio de sistema.
import sys                                          # Módulos de manuseio de sistema.

def sortElements(dataDict, coef=0.2):
    # Função de sorteio de elementos do DB.
    ret_dict = []                                   # Inicia dicionário como vazio.
    idxs = []                                       # Inicia lista de índices.
    tam = len(dataDict)                             # Estima o tamanho total da estrutura.
    num_idx = np.floor(coef*(tam - 1))              # Encontra o número de coficientes do novo dct.
    for ii in range(np.int(num_idx)):               # Itera por cada termo da lista de pacientes.
        tmp = randint(0, tam-1)                     # Sorteia índice aleatório.
        while tmp in idxs:                          # Enquanto tmp está na lista de índices, ...
            tmp = randint(0, tam-1)                 # ... sorteia índice aleatório.

        idxs.append(tmp)                            # Acrescenta o índice sorteado no vetor idxs.

    for idx in idxs:                                # Itera por cada índice do vec de índices.
        ret_dict.append(dataDict[idx])              # Encontra a nova lista com base nos índices sorteados.

    return ret_dict                                 # Retorna a nova estrutura de dados.

def kmeans_plusplus_init(vecList, numPoints):
    # MÉTODO DE INICIALIZAÇÃO DE CENTROIDES KMEANS++
    initCenters = []                                # Inicia centroides iniciais como lista vazia.
    idx = randint(0, vecList.shape[0] - 1)          # Adquire índice de primeiro centroide aleatoriamente.
    initCenters.append(vecList[idx,:])              # Insere centroide sorteado na lista.
    vecList = np.delete(vecList, (idx), axis=0)     # Deleta termo da lista de pacientes.
    prob = np.zeros([vecList.shape[0], 1])          # Inicia vetor de probilidades como espaço vazio.
    while len(initCenters) < numPoints:             # Inicia loop enquanto n se encontram os centroides.
        dist = np.zeros([len(initCenters), 1])      # Inicia vetor de distâncias como espaço vazio.
        for jj in range(vecList.shape[0]):
            pac = vecList[jj,:]
            for ii, center in enumerate(initCenters):
                dist[ii] = norm(pac - center)

            idx = np.argmin(dist)
            center = initCenters[idx]
            prob_sum = 0
            for ii in range(vecList.shape[0]):
                dist_2 = np.zeros([len(initCenters), 1])    # Inicia vetor de distâncias como espaço vazio.
                for jj, center in enumerate(initCenters):
                    dist_2[jj] = norm(vecList[ii,:] - center)

                idx_2 = np.argmin(dist_2)
                prob_sum += norm(initCenters[idx_2] - vecList[ii,:])

            prob[jj] = dist[idx] / prob_sum

        idx = np.argmax(prob)
        initCenters.append(vecList[idx])
        vecList = np.delete(vecList, (idx), axis=0)

    return np.concatenate(initCenters, axis=0)

def recInit(vecList, numPoints, coef=0.1):
    # MÉTODO DE INICIALIZAÇÃO DE CENTROIDES RECURSIVO BASEADO EM K-MEANS DE SUBESPAÇO.
    retCenters = []                                 # Inicia centroides iniciais como lista vazia.
    num_idx = np.floor(coef*(vecList.shape[0] - 1))
    idxs = []
    for ii in range(np.int(num_idx)):
        tmp = randint(0, vecList.shape[0] - 1)
        while tmp in idxs:
            tmp = randint(0, vecList.shape[0] - 1)

        idxs.append(tmp)

    vecList = vecList[idxs,:]

    initCenters = randInit(vecList, numPoints)
    clusters, _ = kMeans_process(vecList, initCenters)
    for clus in clusters:
        retCenters.append(
            np.mean(clus, axis=0)
        )

    return np.concatenate(retCenters, axis=0)

# !! \/ ENTENDER MELHOR O ALGORITMO \/ !!
#def computeRo(vecList, r):
#    # FUNÇÃO DE CÁLCULO DA VAR RO NA INICIALIZAÇÃO CIDP.
#    ro = []                                         # Inicia ro como uma lista vazia.
#    for ii in range(vecList.shape[0]):              # Iter por cada linha da lista de pacientes.
#        var_tmp = 0                                 # Inicia var de iteração como 0.
#        for jj in range(vecList.shape[0]):          # Iter por cada linha da lista de pacientes.
#            if ii == jj: continue                   # Encerra laço se índices jj e ii são iguais.
#            d = norm(vecList[ii,:] - vecList[jj,:]) # Calcula distância entre pacientes ii e jj.
#            var_tmp += np.exp(d/r)                  # Calcula exponencial da soma em questão.
#
#        ro.append(var_tmp)                          # Coloca variável de soma na lista ro.
#
#    return np.array(ro)                             # Retorna var. ro.
#
#def CIDPInit(vecList, numPoints):
#    # FUNÇÃO DE INICIALIZAÇÃO DE CENTROIDES VIA CIDP
#    initCenters = []                                # Inicia estrutura de centros iniciais como vazia.
#    error = 1                                       # Inicia erro como 1. 
#    ro = computeRo(vecList, 1)                      # Calcula vetor "ro".
#    r = ro                                          # Inicia vetor de raios como "ro".
#    while error > 0:                                # Entra em laço até o erro ser nulo.
#
#    return np.concatenate(initCenters, axis=0)      # Retorna os ecntroides iniciais.
# !! /\ ENTENDER MELHOR O ALGORITMO /\ !!

def randInit(vecList, numPoints):
    # FUNÇÃO DE INICIALIZAÇÃO DOS PONTOS COMO PACIENTES ALEATÓRIOS. 
    initCenters = []                                # Inicia estrutura de centros iniciais como vazia.
    for i in range(numPoints):                      # Itera por todos os termos do número de clusters.
        idx = randint(0, len(vecList) - 1)          # Seleciona índice aleatório.
        initCenters.append(vecList[idx,:])          # Acrescenta paciente sorteado na lista de centroides.

    return np.concatenate(initCenters, axis=0)      # Retorna os centroides iniciais.

def initPoints(vecList, numPoints):
    # FUNÇÃO RESPONSÁVEL POR INICIALIZAR OS PONTOS DE CENTRO.
    lim = np.floor(vecList.shape[0]/numPoints)
    lim = np.int(lim)
    initCenters = [] 
    for ii in range(0, len(vecList) - 1, lim):
        initCenters.append(
            np.mean(
                np.matrix(vecList[ii:(ii+lim),:]), 
                axis=0
            )
        )

    return np.concatenate(initCenters, axis=0)      # Retorna pontos identificados.

def supInit(data):
    # FUNÇÃO DE INICIALIZAÇÃO SUPERVISIONADA.
    initCenters = []
    classes = np.unique(data["classes"])
    for cla in classes:
        idxs = np.where(data["classes"] == cla)[0]
        initCenters.append(
            np.mean(data["features"][idxs,:], axis=0)
        )

    return np.concatenate(initCenters, axis=0)      # Retorna pontos identificados.

def kMeans_process(dataVec, initCenters):
    # FUNÇÃO RESPONSÁVEL POR PROCESSAR O ALGORITMO K-MEANS A PARTIR DOS VALORES INICIAIS.
    num_clust = len(initCenters)                    # Encontra número de classes.
    old_centers = np.zeros(initCenters.shape)       # Inicia lista de antigos centros como lista vazia.
    new_centers = initCenters                       # Inicia novos pontos como iniciais.
    error = norm(new_centers - old_centers)         # Calcula a norma sobre as linhas (centros de clusters).
    distance = np.zeros([                           # Inicia matriz de distâncias
        dataVec.shape[0],                           # Inicia matriz de distâncias
        new_centers.shape[0]                        # Inicia matriz de distâncias
    ])
    while error != 0:                               # Repete o loop enquanto erro > 0.
        for i in range(new_centers.shape[0]):       # Itera por cada um dos centroides.
            for j in range(dataVec.shape[0]):       # Itera por cada vetor de dados de cada paciente.
                distance[j, i] = norm(              # Encontra as distâncias por cada centroide.             
                    new_centers[i,:] - dataVec[j,:] # Encontra as distâncias por cada centroide.
                )                                   # Encontra as distâncias por cada centroide.

        clusters = np.argmin(distance, axis=1)      # Encontra em qual cluster o paciente pertence.
        old_centers = new_centers                   # Atualiza velos centros como atuais novos centros.
        clust_dct = {}
        for ii in range(num_clust):
            clust_dct.update({"lst_" + str(ii): []})

        for i, idx in enumerate(clusters):
            for j in range(len(clust_dct)):
                if idx == j: clust_dct[
                    "lst_" + str(j)
                ].append(dataVec[i, :])

        for ii, lst in enumerate(clust_dct.values()):
            if len(lst) == 0:
                # TODO: Verificar a efetivadade desse condicional
                clust_dct["lst_" + str(ii)] = np.zeros(old_centers[0,:].shape)
                continue

            clust_dct["lst_" + str(ii)] = np.concatenate(lst, axis=0)

        for ii, vec in enumerate(clust_dct.values()):
            new_centers[ii,:] = np.mean(
                vec, 
                axis=0
            )

        error = norm(new_centers - old_centers)     # Calcula a norma sobre as linhas (centros de clusters).

    return clust_dct.values(), clusters             # Retorna os clusters calculados

def orgVec(dataDict):
    # FUNÇÃO RESPONSÁVEL POR ORGANIZAR O DICIONÁRIO COM FEATURES EM VALORES NUMÉRICOS.
    dataVec = {"features": [], "classes": []}       # Inicia estrutura de retorno como lista vazia.
    for dct in dataDict:                            # Itera por cada dicionário da lista.
        vals = list(dct.values())                   # Retorna valores do dicionário na lista "vals".   
        for ii, val in enumerate(vals):             # Itera por termos do vetor de valores.
            if val is "": vals[ii] = 0              # Substitui caractere vazio por zero.
            elif val == "False": vals[ii] = 0
            elif val == "True": vals[ii] = 1
            else: vals[ii] = float(val)             # Caso contrário, torna texto em float.
        
        dataVec["features"].append(np.array(vals[1:-2]))    # Apenda o vetor numérico ao fim da lista.
        dataVec["classes"].append(vals[-1])                 # Inclui valor da classe do paciente no campo apropriado.

    dataVec["features"] = np.matrix(dataVec["features"])
    return dataVec                                  # Retorna Lista de listas como lista de vetores numéricos.

def calc_acc(clusters, dataVec, num_clust):
    # FUNÇÃO DE CÁLCULO DE ACURÁCIA PARA A CLASSIFICAÇÃO.
    classes = np.unique(dataVec["classes"])
    class_vec = {cla: 0 for cla in classes}
    conf_mat = []
    global_acc = 0
    tam = 0
    for clust in clusters:
        conf = [0] * num_clust
        for pac in clust:
            idx = np.where(
                (dataVec["features"] == pac).all(1)
            )[0]
            if len(idx) == 0:
                continue
            elif len(idx) > 1:
                idx = idx[0]

            cla = dataVec["classes"][int(idx)]
            class_vec[int(cla)] += 1
            conf[int(cla) - 1] += 1

        max_val = np.max(list(class_vec.values()))
        tot = sum(class_vec.values())
        acc = max_val / tot
        comp = tot - max_val
        cla = np.argmax(list(class_vec.values()))
        cla = list(class_vec.keys())[cla] 
        global_acc += max_val
        tam += tot
        conf_mat.append(conf)
        #print("Acurácia: ", acc)
        #print("Falsos positivos: ", comp)
        #print("Total: ", tot)
        #print("class: ", cla)
        #print("")
    
    #print("Acurácia Global: ", global_acc / tam)
    return global_acc / tam, np.matrix(conf_mat)

def WSSQCalc(clust, dataVec, clust_idx):
    # FUNÇÃO DE CÁLCULO DE MÉTRICA DE INTRASOMAS.
    WSSQ = {str(clus): 0 for clus in range(len(clust))}
    centroids = []
    for lst in clust:
        centroids.append(np.mean(lst, axis=0))

    for idx in clust_idx:
        tmp = norm(
            dataVec[idx,:] - centroids[idx]
        )
        WSSQ[str(idx)] += np.square(tmp)

    return WSSQ

def apply_in_all_data(dataDict, clust):
    # FUNÇÃO DE APLICAÇÃO DOS CENTROS TOTAIS NA BASE INTEIRA.
    dataVec = orgVec(dataDict)
    centers = [] 
    for clus in clust:
        centers.append(np.mean(clus, axis=0))

    dist = np.zeros([dataVec["features"].shape[0], len(centers)])
    for ii in range(dataVec["features"].shape[0]):
        for jj, center in enumerate(centers):
            dist[ii,jj] = norm(
                center - dataVec["features"][ii,:]
            )

    clusters_idx = np.argmin(dist, axis=1)
    return clusters_idx

def main():
    # FUNÇÃO PRINCIPAL DO MÓDULO, OPERA PARA TESTES.
    dataDict_comp = list(csv.DictReader(            # Inicia Lista de dicionários.
        open("../../Breast/data.csv", "r"))          # Inicia Lista de dicionários.
    )
    coef = 0.2
    dataDict = sortElements(dataDict_comp, coef) 
    dataVec = orgVec(dataDict)                      # Organiza a lista de dicionários em lista de vetores.
    dataVec_comp = orgVec(dataDict_comp)            # Organiza estrutura com totalidade da base de dados.
    num_clust = int(max(np.unique(dataVec_comp["classes"]))) + 1 # Define número de clusters.
    #initCenters = initPoints(dataVec["features"], num_clust)    # Determina valores iniciais para 2 vetores.
    #initCenters = recInit(dataVec["features"], num_clust)       # Determina valores iniciais para 2 vetores.
    #initCenters = randInit(dataVec["features"], num_clust)      # Determina valores iniciais para 2 vetores.
    #initCenters = kmeans_plusplus_init(dataVec["features"], num_clust)      # Determina valores iniciais para 2 vetores.
    initCenters = supInit(dataVec)
    clust, clusters_idx = kMeans_process(dataVec["features"], initCenters)  # Processa o algoritmo de clusterização em si.
    clusters_idx = apply_in_all_data(dataDict_comp, clust)
    #print("WSSQ: ", WSSQCalc(clust, dataVec, clust_idx))
    _, conf_mat = calc_acc(clust, dataVec_comp, num_clust)
    #confusion(clusters_idx, dataVec_comp)

if __name__ == "__main__":                          # Verifica se módulo em operação é o principal
    main()                                          # Caso positivo, opera função main().
